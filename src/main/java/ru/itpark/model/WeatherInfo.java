package ru.itpark.model;

public class WeatherInfo {
    private String city;
    private double temperature;

    public WeatherInfo() {
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    @Override
    public String toString() {
        return "WeatherInfo{" +
                "city='" + city + '\'' +
                ", temperature=" + temperature +
                '}';
    }
}
