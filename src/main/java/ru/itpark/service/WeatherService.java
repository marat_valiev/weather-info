package ru.itpark.service;

import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class WeatherService {
    private RestTemplate template = new RestTemplate();

    public double getTemperature(String city) {
        String response = template.getForObject("http://api.openweathermap.org/data/2.5/weather?q=" + city + "&appid=4dafad0b6cc143b1c6230c6af8bc02fe", String.class);
        JSONObject jsonObject = new JSONObject(response);
        JSONObject main = jsonObject.getJSONObject("main");
        return main.getDouble("temp");
    }
}
