package ru.itpark.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.itpark.model.WeatherInfo;
import ru.itpark.service.WeatherService;

@Controller
@RequestMapping("/")
public class Main {
    private final WeatherService service;

    @Autowired
    public Main(WeatherService service) {
        this.service = service;
    }

    @GetMapping
    public String index(Model model) {
        model.addAttribute("title", "Weather Informer");
        return "index";
    }

    @PostMapping("/result")
    public String informer(@ModelAttribute(value = "info") WeatherInfo info) {
        info.setTemperature(service.getTemperature(info.getCity()));

        return "result";
    }
}